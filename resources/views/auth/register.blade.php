@extends('layouts.auth')

@section('content')
<div id="login">
		<aside>
			<figure class="log">
				<a href="index.html"><a href="#">Rudolf History Class</a></a>
			</figure>
      <form method="POST" action="{{ route('register') }}" autocomplete="off">
          @csrf
				<div class="form-group">

					<span class="input">
					<input class="input_field" type="text" name="name">
						<label class="input_label">
						<span class="input__label-content">Your Name</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="email" name="email">
						<label class="input_label">
						<span class="input__label-content">Your Email</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" name="password" type="password" id="password1">
						<label class="input_label">
						<span class="input__label-content">Your password</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" name="password_confirmation" type="password" id="password2">
						<label class="input_label">
						<span class="input__label-content">Confirm password</span>
					</label>
					</span>

					<div id="pass-info" class="clearfix"></div>
				</div>
				<button type="submit" class="btn_1 rounded full-width add_top_30">Register to RHC</button>
				<div class="text-center add_top_10">Already have an acccount? <strong><a href="{{route('login')}}">Sign In</a></strong></div>
			</form>
			<div class="copy">© 2018 RHC</div>
		</aside>
	</div>
@endsection
