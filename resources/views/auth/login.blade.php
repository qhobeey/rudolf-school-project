@extends('layouts.auth')

@section('content')
<div id="login">
  <aside>
    <figure class="log">
      <a href="#">Rudolf History Class</a>
    </figure>
    <form method="POST" action="{{ route('login') }}">
        @csrf
      <div class="access_social">
        <!-- <a href="login.html#0" class="social_bt facebook">Login with Facebook</a>
        <a href="login.html#0" class="social_bt google">Login with Google</a>
        <a href="login.html#0" class="social_bt linkedin">Login with Linkedin</a> -->
      </div>
      <div class="divider"><span>Or</span></div>
      <div class="form-group">
        <span class="input">
        <input class="input_field" type="email" autocomplete="off" name="email">
          <label class="input_label">
          <span class="input__label-content">Your email</span>
        </label>
        </span>

        <span class="input">
        <input class="input_field" type="password" autocomplete="new-password" name="password">
          <label class="input_label">
          <span class="input__label-content">Your password</span>
        </label>
        </span>
        <!-- <small><a href="login.html#0">Forgot password?</a></small> -->
      </div>
      <button class="btn_1 rounded full-width add_top_60">Login to RHC</button>
      <!-- <div class="text-center add_top_10">New to RHC? <strong><a href="{{route('register')}}">Sign up!</a></strong></div> -->
    </form>
    <div class="copy">© 2018 RHC</div>
  </aside>
</div>
@endsection
