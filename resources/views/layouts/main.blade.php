<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Rudolf History Class Application">
    <meta name="author" content="Rudolf Tetteh">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Rudolf History Class | Online academic resources</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- BASE CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
  	<link href="/css/vendors.css" rel="stylesheet">
  	<link href="/css/all_icons.min.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="/css/custom.css" rel="stylesheet">

</head>

<body>

	<div id="page">

	@include('_includes.frontend.nav')
	<!-- /header -->

	@yield('content')
	<!-- /main -->

	@include('_includes.frontend.footer')
	<!--/footer-->
	</div>
	<!-- page -->

    <script src="/js/jquery-2.2.4.min.js"></script>
    <script src="/js/common_scripts.js"></script>
    <script src="/js/main.js"></script>
  	<script src="/js/validate.js"></script>
    <script src="/js/app.js"></script>
    @yield('scripts')

</body>
</html>
