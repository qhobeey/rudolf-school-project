<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ansonika">
  <title>RHC - Admin dashboard</title>

  <!-- Favicons-->
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon" type="image/x-icon" href="/img/favicon.ico">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/img/favicon.ico">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/img/favicon.ico">
  <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/img/favicon.ico">

  <!-- Bootstrap core CSS-->
  <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
  <!-- Main styles -->
  <link href="/admin/css/admin.css" rel="stylesheet">
  <!-- Icon fonts-->
  <link href="/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Plugin styles -->
  <link href="/admin/css/dataTables.bootstrap4.css" rel="stylesheet">

  @yield('styles')
  <!-- Your custom styles -->
  <link href="/admin/css/custom.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer" id="page-top">
  <!-- Navigation-->
  @include('_includes.backend.nav')
  <!-- /Navigation-->
  <div class="content-wrapper">
    <div class="flash-wrapper" style="width: 97%; margin: auto;">
      @include('flash::message')
    </div>

    @yield('content')
	  <!-- /.container-fluid-->
 	</div>
    <!-- /.container-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © RHC 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="index.html#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">Logout
             </a>
             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="/admin/js/jquery.min.js"></script>
    <script src="/admin/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/admin/js/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/admin/js/Chart.js"></script>
    <script src="/admin/js/jquery.dataTables.js"></script>
    <script src="/admin/js/dataTables.bootstrap4.js"></script>
	<script src="/admin/js/jquery.selectbox-0.2.js"></script>
	<script src="/admin/js/retina-replace.min.js"></script>
	<script src="/admin/js/jquery.magnific-popup.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/admin/js/admin.js"></script>
	<!-- Custom scripts for this page-->
    <script src="/admin/js/admin-charts.js"></script>
    <script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    @yield('scripts')

</body>
</html>
