@extends('layouts.main')

@section('content')
<main>
  <section class="hero_single version_2" style="height: 90px;">
  </section>
  <!-- /hero_single -->



  <div class="container margin_30_95">
    <div class="bg_color_1">
  			<div class="container margin_60_35">
  				<div class="main_title_2">
  					<span><em></em></span>
            <h2>Browse all videos</h2>
  				</div>
  				<div class="grid">
  					<ul class="magnific-gallery">
              @foreach($videos as $video)
  						<li style="margin-right: 8px;">
  							<figure>
  								<img src="/images/course_5.jpg" alt="">
  								<figcaption>
  								<div class="caption-content">
  									<a href="{{$video->url}}" class="video" title="{{$video->title}}">
  										<i class="pe-7s-film"></i>
  										<p>{{$video->title}}</p>
  								</a>
  								</div>
  								</figcaption>
  							</figure>
  						</li>
              @endforeach
  					</ul>
  				</div>
  				<!-- /grid -->
  			</div>
  			<!-- /container -->
  		</div>
    <!-- /row -->
  </div>
  <!-- /container -->
</main>
@endsection
