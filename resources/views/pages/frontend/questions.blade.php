@extends('layouts.main')

@section('content')
<main>
  <section class="hero_single version_2" style="height: 90px;">
  </section>
  <!-- /hero_single -->



  <div class="container margin_30_95">
    <div class="bg_color_1">
  			<div class="container margin_60_35">
          <div class="row" v-for="data in questions">
              <div class="col-md-12">
                  <div class="form-group">
                    <label>Question # @{{data.id}} <a href="#0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Accurate question"><i class="fa fa-fw fa-question-circle"></i></a></label>
                      <input type="text" class="form-control" name="question" disabled="true" :value="data.question">
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                    <label>Answer <a href="#0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Accurante answer"><i class="fa fa-fw fa-question-circle"></i></a></label>
                      <input type="text" class="form-control" name="answer" placeholder="Enter Answer" @blur="checkAnswer(data, $event.target.value)">
                  </div>
              </div>
              <div class="col-md-12">
                <div class="success" v-if="response_success">
                  <p>@{{response_msg}}</p>
                </div>
                <div class="failed" v-if="response_error">
                  <p>@{{response_msg}}</p>
                </div>
              </div>
          </div>
  			</div>
  			<!-- /container -->
  		</div>
    <!-- /row -->
  </div>
  <!-- /container -->
</main>
@endsection

@section('scripts')
<script>

    var app = new Vue({
      el: '#page',
      data: {
          questions: [],
          response_msg: '',
          response_error: false,
          response_success: false
      },
      methods: {
        checkAnswer (question, answer) {
          console.log(question.answer.toLowerCase(), answer.toLowerCase())
          if ( answer.toLowerCase() == '' ) {
            this.response_msg = 'please enter an answer'
            this.response_error = true
            this.response_success = false
          }else if (answer.toLowerCase() == question.answer.toLowerCase()) {
            this.response_msg = 'Wow you got the answer correct.'
            this.response_error = false
            this.response_success = true
          }else if (answer.toLowerCase() != question.answer.toLowerCase()) {
            this.response_msg = 'Sorry you got the answer wrong.'
            this.response_error = true
            this.response_success = false
          }else{
            this.response_error = false
            this.response_success = false
          }


        }

      },
      created () {
        axios.get('/api/v1/console/get_all_questions/')
            .then(response => this.questions = response.data.data)
            .catch(error => console.error(error));
      }

    });
</script>
@endsection
