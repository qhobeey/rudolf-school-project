@extends('layouts.main')

@section('content')
<main>
  <section class="hero_single version_2">
    <div class="wrapper">
      <div class="container">
        <h3>What would you learn?</h3>
        <p>Explore more educational resources to help you in your studies</p>
        <form>
          <div id="custom-search-input">
            <div class="input-group">
              <input type="text" class=" search-query" placeholder="Ex. History, Videos, Questions...">
              <input type="submit" class="btn_search" value="Search">
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- /hero_single -->



  <div class="container margin_30_95">
    <div class="bg_color_1">
  			<div class="container margin_60_35">
  				<div class="main_title_2">
  					<span><em></em></span>
            <h2>Free Educational Courses</h2>
            <p>Rudolf History Class provides you with free educational resources for your academics</p>
  				</div>
  				<div class="grid">
  					<ul class="magnific-gallery">
              @foreach($videos as $video)
  						<li style="margin-right: 8px;">
  							<figure>
  								<img src="/images/course_5.jpg" alt="">
  								<figcaption>
  								<div class="caption-content">
  									<a href="{{$video->url}}" class="video" title="{{$video->title}}">
  										<i class="pe-7s-film"></i>
  										<p>{{$video->title}}</p>
  								</a>
  								</div>
  								</figcaption>
  							</figure>
  						</li>
              @endforeach
  					</ul>
  				</div>
  				<!-- /grid -->
  			</div>
  			<!-- /container -->
  		</div>
    <!-- /row -->
  </div>
  <!-- /container -->
</main>
@endsection
