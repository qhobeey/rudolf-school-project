@extends('layouts.admin')
@section('content')
<div class="container-fluid">
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
      <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Add listing</li>
  </ol>

  <form class="" action="{{route('add.questions')}}" method="post">
    @csrf
    <div class="box_general padding_bottom">
        <div class="header_box version_2">
            <h2><i class="fa fa-file-text"></i>Questions</h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="pricing-list-container" style="width:100%;">
                    <tbody>
                        <tr class="pricing-list-item">
                            <td>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                        <label>Question <a href="#0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Accurate question"><i class="fa fa-fw fa-question-circle"></i></a></label>
                                          <input type="text" class="form-control {{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" placeholder="Enter Question">
                                      </div>
                                      @if ($errors->has('question'))
                                          <span class="invalid-feedback">
                                              <strong>{{ $errors->first('question') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                  <div class="col-md-4">
                                      <div class="form-group">
                                        <label>Answer <a href="#0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Accurante answer"><i class="fa fa-fw fa-question-circle"></i></a></label>
                                          <input type="text" class="form-control {{ $errors->has('answer') ? ' is-invalid' : '' }}" name="answer" placeholder="Enter Answer">
                                          @if ($errors->has('answer'))
                                              <span class="invalid-feedback">
                                                  <strong>{{ $errors->first('answer') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="col-md-2">
                                      <div class="form-group">
                                          <!-- <a class="delete" href="#"><i class="fa fa-fw fa-remove"></i></a> -->
                                      </div>
                                  </div>
                              </div>
                            </td>
                        </tr>
                        <tr class="pricing-list-item">
                            <td>

                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- <a href="#0" class="btn_1 gray add-pricing-list-item"><i class="fa fa-fw fa-plus-circle"></i>Add Item</a> -->
            </div>
        </div>
        <!-- /row-->
    </div>
    <!-- /box_general-->
    <p><button type="submit" class="btn_1 medium">Save</button></p>
  </form>
</div>
@endsection
