@extends('layouts.admin')
@section('content')
<div class="container-fluid">
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
      <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Add videos</li>
  </ol>

  <form class="" action="{{route('add.videos')}}" method="post">
    @csrf
    <div class="box_general padding_bottom">
        <div class="header_box version_2">
            <h2><i class="fa fa-video-camera"></i>Videos</h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h6>Item</h6>
                <table id="pricing-list-container" style="width:100%;">
                    <tbody>
                        <tr class="pricing-list-item">
                            <td>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <input type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" placeholder="Video title">
                                      </div>
                                      @if ($errors->has('title'))
                                          <span class="invalid-feedback">
                                              <strong>{{ $errors->first('title') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                                  <div class="col-md-4">
                                      <div class="form-group">
                                          <input type="text" class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" placeholder="Video URL">
                                          @if ($errors->has('url'))
                                              <span class="invalid-feedback">
                                                  <strong>{{ $errors->first('url') }}</strong>
                                              </span>
                                          @endif
                                      </div>
                                  </div>
                                  <div class="col-md-2">
                                      <div class="form-group">
                                          <!-- <a class="delete" href="#"><i class="fa fa-fw fa-remove"></i></a> -->
                                      </div>
                                  </div>
                              </div>
                            </td>
                        </tr>
                        <tr class="pricing-list-item">
                            <td>

                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- <a href="#0" class="btn_1 gray add-pricing-list-item"><i class="fa fa-fw fa-plus-circle"></i>Add Item</a> -->
            </div>
        </div>
        <!-- /row-->
    </div>
    <!-- /box_general-->
    <p><button type="submit" class="btn_1 medium">Save</button></p>
  </form>
</div>
@endsection
