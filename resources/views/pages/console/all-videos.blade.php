@extends('layouts.admin')
@section('styles')
<link href="/css/style.css" rel="stylesheet">
<link href="/css/vendors.css" rel="stylesheet">
<link href="/css/all_icons.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
      <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">All videos</li>
  </ol>

  <div class="box_general padding_bottom">
      <div class="header_box version_2">
          <h2><i class="fa fa-video-camera"></i>Videos</h2>
      </div>
      <div class="grid">
        <ul class="magnific-gallery">
          @foreach($videos as $video)
          <li style="margin-right: 8px;">
            <figure>
              <img src="/images/course_5.jpg" alt="">
              <figcaption>
              <div class="caption-content">
                <a href="{{$video->url}}" target="_blank" class="video" title="Video Vimeo">
                  <i class="pe-7s-film"></i>
                  <p>{{$video->title}}</p>
              </a>
              </div>
              </figcaption>
            </figure>
          </li>
          @endforeach
        </ul>
      </div>
      <!-- /row-->
  </div>
</div>
@endsection
<!-- @if(\Session::has('error'))
                        <p class="error-p" style="color: red; text-align: center;">{!! \Session::get('error') !!}</p>
                    @endif -->
