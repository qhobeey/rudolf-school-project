<footer>
  <div class="container footer-paddings">
    <div class="row">
      <div class="col-md-8">
        <ul id="additional_links">
          <li><a href="#">Terms and conditions</a></li>
          <li><a href="#">Privacy</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <div id="copy">© 2018 Rudolf History Class</div>
      </div>
    </div>
  </div>
</footer>
