<header class="header menu_2">
  <div id="preloader"><div data-loader="circle-side"></div></div><!-- /Preload -->
  <div id="logo">
    <!-- <a href="index.html"><img src="img/logo.png" width="149" height="42" data-retina="true" alt=""></a> -->
    <a href="{{route('i.home')}}">Rudolf History Class</a>
  </div>
  <ul id="top_menu">
    <li><a href="#" class="login">Login</a></li>
    <li><a href="#" class="search-overlay-menu-btn">Search</a></li>
    <li class="hidden_tablet"><a href="{{route('login')}}" class="btn_1 rounded">Login account</a></li>
  </ul>
  <!-- /top_menu -->
  <a href="index.html#menu" class="btn_mobile">
    <div class="hamburger hamburger--spin" id="hamburger">
      <div class="hamburger-box">
        <div class="hamburger-inner"></div>
      </div>
    </div>
  </a>
  <nav id="menu" class="main-menu">
    <ul>
      <li><span><a href="{{route('i.home')}}">Home</a></span></li>
      <li><span><a href="{{route('i.questions')}}">Questions</a></span></li>
      <li><span><a href="{{route('i.videos')}}">Videos</a></span></li>
    </ul>
  </nav>
  <!-- Search Menu -->
  <div class="search-overlay-menu">
    <span class="search-overlay-close"><span class="closebt"><i class="ti-close"></i></span></span>
    <form role="search" id="searchform" method="get">
      <input value="" name="q" type="search" placeholder="Search..." />
      <button type="submit"><i class="icon_search"></i>
      </button>
    </form>
  </div><!-- End Search Menu -->
</header>
