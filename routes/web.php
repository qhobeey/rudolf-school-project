<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@home')->name('i.home');
Route::get('videos/library', 'FrontendController@videos')->name('i.videos');
Route::get('questions/library', 'FrontendController@questions')->name('i.questions');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('console')->group(function () {
    Route::get('/', 'ConsoleController@index')->name('console');
    Route::get('add-questions', 'ConsoleController@addQuestions')->name('add.questions');
    Route::get('add-videos', 'ConsoleController@addVideos')->name('add.videos');
    Route::get('all-videos', 'ConsoleController@allVideos')->name('all.videos');
    Route::post('add-videos', 'ConsoleController@saveVideos')->name('add.videos');
    Route::post('add-questions', 'ConsoleController@saveQuestions')->name('add.questions');
});

Route::prefix('api/v1/console')->group(function () {
    Route::get('get_all_questions', 'FrontendController@getAllQuestions');
});
