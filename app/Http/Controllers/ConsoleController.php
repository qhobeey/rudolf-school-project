<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Question;

class ConsoleController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index()
    {
      return view('pages.console.home');
    }

    public function addQuestions()
    {
      return view('pages.console.add-questions');
    }

    public function addVideos()
    {
      return view('pages.console.add-videos');
    }
    public function allVideos()
    {
      $videos = Video::latest()->get();
      return view('pages.console.all-videos', compact('videos'));
    }

    public function saveVideos(Request $request)
    {
      // $data = $request->validate(['*.title' => 'required|unique:videos', '*.url' => 'required|unique:videos']);
      $data = $request->validate(['title' => 'required|unique:videos', 'url' => 'required|unique:videos']);
      Video::create($data);
      flash('Video data successfully saved!')->success();
      return redirect()->back();
    }

    public function saveQuestions(Request $request)
    {
      // $data = $request->validate(['*.title' => 'required|unique:videos', '*.url' => 'required|unique:videos']);
      $data = $request->validate(['question' => 'required|unique:questions', 'answer' => 'required|unique:questions']);
      Question::create($data);
      flash('Question data successfully saved!')->success();
      return redirect()->back();
    }

    
}
