<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Question;

class FrontendController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function home()
    {
      $videos = Video::latest()->get();
      return view('pages.frontend.index', compact('videos'));
    }

    public function videos()
    {
      $videos = Video::latest()->get();
      return view('pages.frontend.videos', compact('videos'));
    }

    public function questions()
    {
      return view('pages.frontend.questions');
    }

    public function getAllQuestions()
    {
        $data = Question::latest()->get();
        return response()->json(['status' => 'success', 'data' => $data]);
    }
}
